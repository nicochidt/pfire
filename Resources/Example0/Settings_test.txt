# than comments are only set by a leading pound sign
# and projectfilename really should be the very first item 
# in this file
# please leave all the other items landscapefile adjustments file 
# etc empty unless you want to override the project file. 
#projectfilename =/home/elena/backupfarsitemarch28/trunk/ashley1.fpj

version			= 43
adjustmentFile 		= /home/lpelegrin/Simulacions/CARDONA5/Input/GIS_themes/ashley.adj
fuelmoisturefile 	= /home/lpelegrin/Simulacions/CARDONA5/Input/GIS_themes/Aug11.fms
landscapeFile		= /home/lpelegrin/Simulacions/CARDONA5/Input/ASCII_files/Cardona.lcp
weatherFile0 		= /home/lpelegrin/Simulacions/CARDONA5/Input/GIS_themes/cardona.wtr
windFile0 		= /home/lpelegrin/Simulacions/CARDONA5/Input/GIS_themes/cardona.ATM
timestep 		= 10m
visibleStep 		= 10m
secondaryVisibleStep 	= 48h
perimeterResolution 	= 30m
distanceResolution 	= 30m

enableCrownfire 		= false
linkCrownDensityAndCover 	= false
embersFromTorchingTrees 	= false
enableSpotFireGrowth 		= false
nwnsBackingROS 			= false
fireacceleration		= false
accelerationtranstion		= 1m
distanceChecking 		= fireLevel
simulatePostFrontalCombustion 	= false
fuelInputOption 		= absent
calculationPrecision 		= normal
useConditioningPeriod 		= false 
ignitionFile 			= /home/lpelegrin/Simulacions/CARDONA5/Input/GIS_themes/Ignition.shp
ignitionFileType 		= point

####fuelmodelfile=/home/user/Documentos/Fases2/FarsiteSIM/Simulacio1Lucas/Input/GIS_themes/ashley.FMD
#fuelmodelfile =/home/tomas/Carlos/ORIGINAL/core/Input/ashley.FMD 
#this should be east and then north white space delimited
#note that gridrow and east north must both be set

StartMonth 	= 8
StartDay 	= 11
StartHour 	= 1200
StartMin 	= 00
EndMonth	= 8
EndDay 		= 11
EndHour 	= 1800
EndMin 		= 00
RastMake 	= true
RasterFileName 	= /home/lpelegrin/Simulacions/CARDONA5/Output/raster
VectMake 	= true
VectorFileName 	= /home/lpelegrin/Simulacions/CARDONA5/Output/vector.VCT
ShapeMake 	= true
shapefile 	= /home/lpelegrin/Simulacions/CARDONA5/Output/shape.shp

RAST_ARRIVALTIME 	= true
RAST_FIREINTENSITY 	= false
RAST_SPREADRATE 	= false
RAST_FLAMELENGTH 	= false
RAST_HEATPERAREA 	= false
RAST_CROWNFIRE 		= false
RAST_FIREDIRECTION 	= true
RAST_REACTIONINTENSITY 	= false
