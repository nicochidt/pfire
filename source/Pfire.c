/*==============================================================================================================
    PFire Simulator - Pfire.c
=================================================================================================================
    PFire it's a forest fire simulator developed to be executed in multi many core environments.
    It's a project that born at (ACSO/CAOS) department in the Unversity Autonomous of Barcelona (UAB).
=================================================================================================================
    Version: V0 (in progress...) Betta Version in Serial execution
=================================================================================================================
    Authors:
        Àngel Farguell
        Nicolas Chiaraviglio
        Lucas Pelegrin
===============================================================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Pfire.h"
#include "BaseMap.h"
/*=================================================================================================================
    Function:   main

    Description:
    Main loop of the simulator.
=================================================================================================================*/
int main(int argc, char const *argv[]) {
    struct BASEMAP BaseMap;
    struct SIMULATIONSETTINGS SimulationSettings;

    PFire_SettingsInitialization(argc,argv, &SimulationSettings);
    printf("PFire: Settings Initialization[Ok]\n");
    BaseMap_Initialization(&BaseMap,SimulationSettings.FilePath_Elevation);
    printf("PFire: BaseMap Initialization[OK]\n");


    // Refinar Perimetro
    // Calcular Ro

    //While ( dt < tt ){
    // PerimeterExpansion()
    // CrossSolver()
    // PerimeterRediscretization()
    // }
    //PrintOutput()


    Pfire_FreeSettings(&SimulationSettings);
    BaseMap_BaseMapFreeMemory(&BaseMap);

    printf("Pfire: Ends Successfully\n");
    return 0;
}
/*=================================================================================================================
    Function:   PFire_SettingsInitialization

    INCOMPLETED

    Description:
    Sets and parse the simulation paremeters recived in command line.
=================================================================================================================*/
void PFire_SettingsInitialization(int argc, char *argv[],struct SIMULATIONSETTINGS *Settings) {
    int count;
    char Error;
    int Buffersize = argc * (sizeof(char) * MAXBUFFERSIZE );
    char *Buffer_parameter;
    char *Buffer_data;
    Buffer_parameter = (char *)malloc(Buffersize);
    Buffer_data      = (char *)malloc(Buffersize);


    for (count = 1; count < argc ; count = count + 2)
    {
        Error = 1;
        //REMOVE DATA FROM BUFFERS
        memset(Buffer_parameter,'\0',Buffersize );
        memset(Buffer_data     ,'\0',Buffersize );
        //COPY NEW INFORMATION
        strcpy(Buffer_parameter,argv[count]     );
        strcpy(Buffer_data     ,argv[count + 1 ]);

        if ( !strcmp(Buffer_parameter,"-h") ){ // HELP
            Pfire_printhelp();
        }
        if ( !strcmp(Buffer_parameter,"-tt") ){ // TOTAL SIMULATION TIME
            Error = 0;
            sscanf(Buffer_data,"%d", &Settings->TotalSimulationTime);
        }
        if ( !strcmp(Buffer_parameter,"-dt") ){ // STEP SIMULATION TIME
            Error = 0;
            sscanf(Buffer_data,"%d", &Settings->StepSimulationTime);
        }
        if ( !strcmp(Buffer_parameter,"-st") ){ // INITIAL SIMULATION TIME
            Error = 0;
            sscanf(Buffer_data,"%d", &Settings->InitialSimulationTime);
        }
        if ( !strcmp(Buffer_parameter,"-ef") ){ // ELEVATION FILE PATH
            Error = 0;
            Settings->FilePath_Elevation = (char *)malloc(Buffersize);
            memset(Settings->FilePath_Elevation,'\0',sizeof(Buffersize) );
            strcpy(Settings->FilePath_Elevation, Buffer_data);
        }
        if ( !strcmp(Buffer_parameter,"-fm") ){ // FUEL MODEL
            Error = 0;
        }
        if ( !strcmp(Buffer_parameter,"-np") ){ // NUMBER OF INTERMEDIATE PERIMETERS
            Error = 0;
        }
        if ( !strcmp(Buffer_parameter,"-S") ){ // SETTINGS FILE
            Error = 0;
        }
        if ( !strcmp(Buffer_parameter,"-I") ){ // INPUT DIRECTORY
            Error = 0;
        }
        if ( !strcmp(Buffer_parameter,"-O") ){ // OUTPUT DIRECTORY
            Error = 0;
        }
        if ( Error )
        {
            printf("ARGUMENT INVALID:\n");
            printf("%s %s\n",Buffer_parameter, Buffer_data);
            exit(-1);
        }
    }
}
/*=================================================================================================================
    Function:   Pfire_FreeSettings

    INCOMPLETED

    Description:
     Set free the memory allocated in the Initialization part.
=================================================================================================================*/
void Pfire_FreeSettings(struct SIMULATIONSETTINGS *Settings){
    free(Settings->FilePath_Elevation);
    //free(Settings->FilePath_Weather);
    //free(Settings->FilePath_Wind);
}
/*=================================================================================================================
    Function:   Pfire_printhelp

    INCOMPLETED

    Description:
    Prints the help information
=================================================================================================================*/
void Pfire_printhelp(){
    printf("============================================================================================\n");
    printf("Pfire - help                                                                                \n");
    printf("============================================================================================\n");
    printf("Valid Arguments:                                                                            \n");
    printf(" -h: show help information                                                                  \n");
    printf("============================================================================================\n");
}