/*==============================================================================================================
    PFire Simulator - Pfire.h
=================================================================================================================
    PFire it's a forest fire simulator developed to be executed in multi many core environments.
    It's a project that born at (ACSO/CAOS) department in the Unversity Autonomous of Barcelona (UAB).
=================================================================================================================
    Version: V0 (in progress...) Betta Version in Serial execution
=================================================================================================================
    Authors:
        Àngel Farguell
        Nicolas Chiaraviglio
        Lucas Pelegrin
===============================================================================================================*/
#ifndef PFIRE_PFIRE_H
#define PFIRE_PFIRE_H
/*===============================================================================================================
    Declarations
===============================================================================================================*/
#define MAXBUFFERSIZE 100       //Max Buffer size to read from file in the initialization functions.
struct SIMULATIONSETTINGS {
    int TotalSimulationTime;
    int StepSimulationTime;
    int InitialSimulationTime;
    //FilePaths
    char *FilePath_Elevation;
    char *FilePath_Wind;
    char *FilePath_Weather;
} ;
/*===============================================================================================================
    Functions
===============================================================================================================*/
void PFire_SettingsInitialization(int argc, char *argv[], struct SIMULATIONSETTINGS *Settings);
void Pfire_FreeSettings(struct SIMULATIONSETTINGS *Settings);
void Pfire_printhelp();
#endif //PFIRE_PFIRE_H
