#pragma once
#include <stdlib.h>
#pragma once
#include "fuelModel.h"


float ComputeROS0 (FUELMODEL);
float *ComputeROS  (float ROS0, float * normal, float *slope, float *wind);
float SlopeFactor (FUELMODEL, float phi);
float WindFactor (FUELMODEL, float kk);
float Computexi (float sigma, float beta);
float ComputeGamma (float Gammamax, float beta, float betaop, float A);
float ComputeIR (float Gamma, float omegan, float h, float etaM, float etas);
float ComputeROS0_ISU (struct FuelModel);
float Computexi_ISU (float sigma, float beta);
int FuelModel (FUELMODEL *FuelModel, int type);
