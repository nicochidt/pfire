/*==============================================================================================================
    PFire Simulator - BaseMap.h
=================================================================================================================
    PFire it's a forest fire simulator developed to be executed in multi many core environments.
    It's a project that born at (ACSO/CAOS) department in the Unversity Autonomous of Barcelona (UAB).
=================================================================================================================
    Version: V0 (in progress...) Betta Version in Serial execution
=================================================================================================================
    Authors:
        Àngel Farguell
        Nicolas Chiaraviglio
        Lucas Pelegrin
===============================================================================================================*/
#ifndef PFIRE_BASEMAP_H
#define PFIRE_BASEMAP_H
/*===============================================================================================================
    Declarations
===============================================================================================================*/
#define MAXBUFFERSIZE 100             //Max Buffer size to read from file in the initialization functions.
#define MAXHEADERLINESELEVATIONFILE 6 //Number of headers in the input files.
struct BASEMAP {
    //Common data for all maps
    int     X_Cells;
    int     Y_Cells;
    double  X_Position;
    double  Y_Position;
    float   CellSize;
    int     NoDataValue;
    //Input Static Data Maps
    int     *ElevationMap;
    float   *SlopeMap;
    float   *AspectMap;
    int     *FuelMap;
    //Input Dinamic Data Maps
    float   *WindMap_Direction;
    float   *WindMap_Speed;
    float   *WeatherMap;
    //Simulation Time Map Information
    float   *ROSMap;
};
/*===============================================================================================================
    Functions
===============================================================================================================*/
void BaseMap_Initialization(struct BASEMAP *BaseMap,char *Elevation_file);
void BaseMap_ElevationMapInitialization(struct BASEMAP *BaseMap,char *Elevation_file);
void BaseMap_SlopeMapInitialization(struct BASEMAP *BaseMap);
void BaseMap_AspectMapInitialization(struct BASEMAP *BaseMap);
void BaseMap_FuelMapInitialization(struct BASEMAP *BaseMap);
void BaseMap_BaseMapFreeMemory(struct BASEMAP *BaseMap);
#endif //PFIRE_BASEMAP_H
