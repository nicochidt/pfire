/*==============================================================================================================
 PFire Simulator - ros.c
 =================================================================================================================
 PFire it's a forest fire simulator developed to be executed in multi many core environments.
 It's a project that born at (ACSO/CAOS) department in the Unversity Autonomous of Barcelona (UAB).
 =================================================================================================================
 Version: V0 (in progress...) Betta Version in Serial execution
 =================================================================================================================
 Authors:
 Àngel Farguell
 Nicolas Chiaraviglio
 Lucas Pelegrin
 ===============================================================================================================*/
//#pragma once
#ifndef STDLIB_H
#include <stdlib.h>
#endif

#ifndef STDIO_H
#include <stdio.h>
#endif

#ifndef MATH_H
#include <math.h>
#endif

#include "ros.h"

/*=================================================================================================================
 Function:   ComputeROS0
 
 COMPLETED
 
 Description:
 Compute R_0 using the variables from FuelModel.
 =================================================================================================================*/
float ComputeROS0 (FUELMODEL FuelModel)
{
    printf("COMPUTE IMPERIAL UNIT EQUATIONS\n");
    // Convertion factors
    float mtoft = 3.280839895;          // 1 m = ? ft
    float kgtolb = 2.204622621849;      // 1 Kg = ? lb
    float BtutoJ = 1055.06;             // 1 Btu = ? J
    float mintos = 60;                  // 1 min = ? s
    
    // Compute R_0 using the variables from FuelModel
    // Redifine the variables from FuelModel
    float sigma = FuelModel.fuelSAV; // --> 1/ft
    float omegal = FuelModel.fuelLoad; // --> lb/ft^2
    float Mf = FuelModel.fuelMoistCont; // --> 1
    float Mx = FuelModel.mExt; // --> 1
    float ST = FuelModel.fuelTotMinCont; // --> 1
    float Se = FuelModel.fuelEfMinCont; // --> 1
    float rhoP = FuelModel.fuelOvDensity; // --> lb/ft^3
    float delta = FuelModel.fuelDepth; // --> ft
    float h = FuelModel.fuelHeatContDead; // --> B.t.u./lb

    // Compute constants from FuelModel
    float epsilon = exp(-138/sigma);                            // Effective heating number (1)
    float omega0 = omegal/(1+Mf);                               // Total fuel load net of moisture (lb/ft^2)
    float betaop = 3.348*pow(sigma,-0.8189);                    // Optimum packing ratio (1)
    float Gammamax = pow(sigma,1.5)/(495+0.594*pow(sigma,1.5)); // Maximum reaction velocity (1/min)
    float A = 1/(4.774*pow(sigma,0.1)-7.27);                     // Constant (1)
    float mr = Mf/Mx;                                           // Fuel moisture ratio (1)
    float etaM = 1-2.59*mr+5.11*pow(mr,2)+3.52*pow(mr,3);       // Moisture damping coefficient (1)
    float etas = 0.174*pow(Se,-0.19);                           // Mineral damping coefficient (1)
    printf("epsilon: %f -- ", epsilon);
    printf("omega0: %f -- ", omega0*pow(mtoft,2)/kgtolb);
    printf("betaop: %f -- ", betaop);
    printf("Gammamax: %f -- ", Gammamax/mintos);
    printf("A: %f -- ", A);
    printf("mr: %f -- ", mr);
    printf("etaM: %f -- ", etaM);
    printf("etaM: %f\n", etas);
    
    // Compute some important relations between variables
    float rhob = omega0/delta;                                  // Oven dry bulk density (lb/ft^3)
    float omegan = omega0/(1+ST);                               // Fuel loading net of minerals (lb/ft^2)
    float beta = rhob/rhoP;                                     // Packing ratio (1)
    float Qig = 250*beta+1116*Mf;                               // Heat of preignition (B.t.u./lb)
    float xi = Computexi(sigma,beta);                           // Propagating flux ratio (1)
    float Gamma = ComputeGamma(Gammamax,beta,betaop,A);         // Optimum reaction velocity (1/min)
    float IR = ComputeIR(Gamma,omegan,h,etaM,etas);             // Reaction Intensity (B.t.u./(ft^2·min))
    printf("rhob: %f -- ", rhob*pow(mtoft,3)/kgtolb);
    printf("omegan: %f -- ", omegan*pow(mtoft,2)/kgtolb);
    printf("beta: %f -- ", beta);
    printf("Qig: %f -- ", Qig*BtutoJ*kgtolb);
    printf("xi: %f -- ", xi);
    printf("Gamma: %f -- ", Gamma/mintos);
    printf("IR: %f\n", IR*BtutoJ*pow(mtoft,2)/mintos);
    
    float R0 = IR*xi/(rhob*epsilon*Qig);                                // Rate of spread without wind and slope (m/s)
    printf("R0: %f\n",R0/mtoft/mintos);
    
    // Compute R_0
    return R0;
}

/*=================================================================================================================
 Function:   ComputeROS0_ISU
 
 COMPLETED
 
 Description:
 Compute R_0 using the variables from FuelModel in the International System of Units.
 =================================================================================================================*/
float ComputeROS0_ISU (FUELMODEL FuelModel)
{
    printf("COMPUTE ISU EQUATIONS\n");
    // Compute R_0 using the variables from FuelModel
    // Redifine the variables from FuelModel
    float sigma = FuelModel.fuelSAV; // --> 1/m
    float omegal = FuelModel.fuelLoad; // --> Kg/m^2
    float Mf = FuelModel.fuelMoistCont; // --> 1
    float Mx = FuelModel.mExt; // --> 1
    float ST = FuelModel.fuelTotMinCont; // --> 1
    float Se = FuelModel.fuelEfMinCont; // --> 1
    float rhoP = FuelModel.fuelOvDensity; // --> Kg/m^3
    float delta = FuelModel.fuelDepth; // --> m
    float h = FuelModel.fuelHeatContDead; // --> J/Kg
    
    // Used convertion factors from Google
    // 1 m = 3.280839895 ft
    // 1 kg = 2.204622621849 lb
    // 1 Btu = 1055.06 J
    
    // Compute constants from FuelModel
    float epsilon = exp(-452.75590551/sigma);                            // Effective heating number (1)
    float omega0 = omegal/(1+Mf);                                        // Total fuel load net of moisture (Kg/m^2)
    float betaop = 8.8577944164*pow(sigma,-0.8189);                      // Optimum packing ratio (1)
    float Gammamax = pow(sigma,1.5)/(176495.6497+35.64*pow(sigma,1.5));  // Maximum reaction velocity (1/s)
    float A = 1/(4.2392000432*pow(sigma,0.1)-7.27);                      // Constant (1)
    float mr = Mf/Mx;                                                    // Fuel moisture ratio (1)
    float etaM = 1-2.59*mr+5.11*pow(mr,2)+3.52*pow(mr,3);                // Moisture damping coefficient (1)
    float etas = 0.174*pow(Se,-0.19);                                    // Mineral damping coefficient (1)
    printf("epsilon: %f -- ", epsilon);
    printf("omega0: %f -- ", omega0);
    printf("betaop: %f -- ", betaop);
    printf("Gammamax: %f -- ", Gammamax);
    printf("A: %f -- ", A);
    printf("mr: %f -- ", mr);
    printf("etaM: %f -- ", etaM);
    printf("etaM: %f\n", etas);
    
    // Compute some important relations between variables
    float rhob = omega0/delta;                                          // Oven dry bulk density (Kg/m^3)
    float omegan = omega0/(1+ST);                                       // Fuel loading net of minerals (Kg/m^2)
    float beta = rhob/rhoP;                                             // Packing ratio (1)
    float Qig = 581502.28585*beta+2595826.204*Mf;                       // Heat of preignition (J/Kg)
    float xi = Computexi_ISU(sigma,beta);                               // Propagating flux ratio (1)
    float Gamma = ComputeGamma(Gammamax,beta,betaop,A);                 // Optimum reaction velocity (1/s)
    float IR = ComputeIR(Gamma,omegan,h,etaM,etas);                     // Reaction Intensity (J/(m^2·s))
    printf("rhob: %f -- ", rhob);
    printf("omegan: %f -- ", omegan);
    printf("beta: %f -- ", beta);
    printf("Qig: %f -- ", Qig);
    printf("xi: %f -- ", xi);
    printf("Gamma: %f -- ", Gamma);
    printf("IR: %f\n", IR);
    
    float R0 = IR*xi/(rhob*epsilon*Qig);                                // Rate of spread without wind and slope (m/s)
    printf("R0_ISU: %f\n",R0);
    
    // Compute R_0
    return R0;
}

/*=================================================================================================================
 Function:   Computexi
 
 COMPLETED
 
 Description:
 Compute the propagation flux ratio (\xi).
 =================================================================================================================*/
float Computexi (float sigma, float beta)
{
    return exp((0.792+0.681*pow(sigma,0.5))*(beta+0.1))/(192+0.2595*sigma);
}

/*=================================================================================================================
 Function:   Computexi_ISU
 
 COMPLETED
 
 Description:
 Compute the propagation flux ratio (\xi) in the International System of Units.
 =================================================================================================================*/
float Computexi_ISU (float sigma, float beta)
{
    return exp((0.792+0.3759712127*pow(sigma,0.5))*(beta+0.1))/(192+0.0790956*sigma);
}

/*=================================================================================================================
 Function:   ComputeGamma
 
 COMPLETED
 
 Description:
 Compute the optimum reaction velocity (\Gamma).
 =================================================================================================================*/
float ComputeGamma (float Gammamax, float beta, float betaop, float A)
{
    float betaratio = beta/betaop;
    return Gammamax*pow(betaratio,A)*exp(A*(1-betaratio));
}

/*=================================================================================================================
 Function:   ComputeIR
 
 COMPLETED
 
 Description:
 Compute the reaction intensity (I_R).
 =================================================================================================================*/
float ComputeIR (float Gamma, float omegan, float h, float etaM, float etas)
{
    float IR = h*Gamma;
    IR = IR*omegan*etaM*etas;
    return IR;
}

/*=================================================================================================================
 Function:   FuelModel
 
 COMPLETED
 
 Description:
 Define the coefficients of the type Fuel Model into the Fuel Model struct.
 =================================================================================================================*/
int FuelModel (FUELMODEL *FuelModel, int type)
 {
     FuelModel->fuelHeatContDead = 17433000; // h
     FuelModel->fuelMoistCont = 0.080000; // Mf
     
     switch (type) {
         case 1:
             printf("--> Short grass (1 ft)\n");
             FuelModel->fuelSAV = 3500.0; // sigma
             FuelModel->fuelLoad = 0.16600; // omegal
             FuelModel->mExt = 0.12000; // Mx
             FuelModel->fuelTotMinCont = 0.055500; // ST
             FuelModel->fuelEfMinCont = 0.010000; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.30500; // delta
             break;
         case 2:
             printf("--> Timber (grass and understory)\n");
             FuelModel->fuelSAV = 2784.0; // sigma
             FuelModel->fuelLoad = 0.89700; // omegal
             FuelModel->mExt = 0.15000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.30500; // delta
             break;
         case 3:
             printf("--> Tall grass (2.5 ft)\n");
             FuelModel->fuelSAV = 1500.0; // sigma
             FuelModel->fuelLoad = 0.67500; // omegal
             FuelModel->mExt = 0.25000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.76200; // delta
             break;
         case 4:
             printf("--> Chaparral (6 ft)\n");
             FuelModel->fuelSAV = 1739.0; // sigma
             FuelModel->fuelLoad = 2.4680; // omegal
             FuelModel->mExt = 0.20000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 1.8290; // delta
             break;
         case 5:
             printf("--> Brush (2 ft)\n");
             FuelModel->fuelSAV = 1683.0; // sigma
             FuelModel->fuelLoad = 0.78500; // omegal
             FuelModel->mExt = 0.20000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.61000; // delta
             break;
         case 6:
             printf("--> Dormant brush, hardwood slash\n");
             FuelModel->fuelSAV = 1564.0; // sigma
             FuelModel->fuelLoad = 1.3450; // omegal
             FuelModel->mExt = 0.25000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.76200; // delta
             break;
         case 7:
             printf("--> Southern rough\n");
             FuelModel->fuelSAV = 1562.0; // sigma
             FuelModel->fuelLoad = 1.0920; // omegal
             FuelModel->mExt = 0.40000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.76200; // delta
             break;
         case 8:
             printf("--> Closed timber litter\n");
             FuelModel->fuelSAV = 1889.0; // sigma
             FuelModel->fuelLoad = 1.1210; // omegal
             FuelModel->mExt = 0.30000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.061000; // delta
             break;
         case 9:
             printf("--> Hardwood litter\n");
             FuelModel->fuelSAV = 2484.0; // sigma
             FuelModel->fuelLoad = 0.78000; // omegal
             FuelModel->mExt = 0.25000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.061000; // delta
             break;
         case 10:
             printf("--> Timber (litter + understory)\n");
             FuelModel->fuelSAV = 1764.0; // sigma
             FuelModel->fuelLoad = 2.6940; // omegal
             FuelModel->mExt = 0.25000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.30500; // delta
             break;
         case 11:
             printf("-->  logging slash\n");
             FuelModel->fuelSAV = 1182.0; // sigma
             FuelModel->fuelLoad = 2.5820; // omegal
             FuelModel->mExt = 0.15000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.30500; // delta
             break;
         case 12:
             printf("--> Medium logging slash\n");
             FuelModel->fuelSAV = 1145.0; // sigma
             FuelModel->fuelLoad = 7.7490; // omegal
             FuelModel->mExt = 0.20000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.70100; // delta
             break;
         case 13:
             printf("--> Heavy logging slash\n");
             FuelModel->fuelSAV = 1159.0; // sigma
             FuelModel->fuelLoad = 13.024; // omegal
             FuelModel->mExt = 0.25000; // Mx
             FuelModel->fuelTotMinCont = 0.0555; // ST
             FuelModel->fuelEfMinCont = 0.010; // Se
             FuelModel->fuelOvDensity = 32.000; // rhoP
             FuelModel->fuelDepth = 0.91400; // delta
             break;
         default:
             printf("Error: It is not a valid Fuel Model.\n");
             return 1;
     }
     return 0;
 }

/*=================================================================================================================
 Function:   ComputeROS
 
 INCOMPLETED
 
 Description:
 Compute R_0 from the variables in FuelModel.
 =================================================================================================================*/
/*float *ComputeROS (float ROS0, float *normal, float *slope, float *wind)
{	
	// Compute Rothermel formula: R = R_0 * (1 + \Phi_s + \Phi_w)
	// 1 is the normal direction to the curve
	float ros[2];
	ros[0] = ROS0 * (normal[0] + slope[0] + wind[0]);
	ros[1] = ROS0 * (normal[1] + slope[1] + wind[1]);
	return ros;

}*/



/*=================================================================================================================
 Function:   SlopeFactor
 
 INCOMPLETED
 
 Description:
 Compute R_0 from the variables in FuelModel.
 =================================================================================================================*/
/*float SlopeFactor (FUELMODEL FuelModel, float phi)
{	
	// Slope factor for the Rothermel formula: \Phi_s = 5.275 * \beta ^ 0.3 * (tan \Phi) ^ 2
	float beta = FuelModel.fuelOvDensity/FuelModel.fuelOvDensity;
	// quizas falta un operador de proyeccion?
	float aux = tan(phi);
	return 5.27 * pow(beta, 0.3) * aux * aux;

}*/

/*=================================================================================================================
 Function:   WindFactor
 
 INCOMPLETED
 
 Description:
 Compute R_0 from the variables in FuelModel.
 =================================================================================================================*/
/*float WindFactor (FUELMODEL FuelModel, float kk)
{
    
}*/

int main(int argc, char *argv[])
{
    if (argc!=2){
        printf("%s fuel_model_number \n", argv[0]);
        return 1;
    }
    int nmodel = atoi(argv[1]);
    
    FUELMODEL FModel;        // The fuel model in the Imperial Units.
    FUELMODEL FModel_ISU;    // The fuel model in the International System of Units (ISU).
    
    int error = FuelModel(&FModel,nmodel);
    if (error == 0){
        //printf("Fuel Model 3 Input: %f, %f, %f, %f, %f, %f, %f, %f, %f\n", FModel.fuelSAV, FModel.fuelLoad, FModel.fuelMoistCont, FModel.mExt, FModel.fuelTotMinCont, FModel.fuelEfMinCont, FModel.fuelOvDensity, FModel.fuelDepth, FModel.fuelHeatContDead);
    
        // Convertion factors
        float mtoft = 3.280839895;          // 1 m = ? ft
        float kgtolb = 2.204622621849;      // 1 Kg = ? lb
        float BtutoJ = 1055.06;             // 1 Btu = ? J
        float mintos = 60;                  // 1 min = ? s
    
        // Fuel model in ISU
        FModel_ISU.fuelSAV = FModel.fuelSAV * mtoft; // sigma --> 1/m
        FModel_ISU.fuelLoad = FModel.fuelLoad / kgtolb * pow(mtoft,2); // omegal --> Kg/m^2
        FModel_ISU.fuelMoistCont = FModel.fuelMoistCont; // Mf --> 1
        FModel_ISU.mExt = FModel.mExt; // Mx --> 1
        FModel_ISU.fuelTotMinCont = FModel.fuelTotMinCont; // ST --> 1
        FModel_ISU.fuelEfMinCont = FModel.fuelEfMinCont; // Se --> 1
        FModel_ISU.fuelOvDensity = FModel.fuelOvDensity / kgtolb * pow(mtoft,3); // rhoP --> Kg/m^3
        FModel_ISU.fuelDepth = FModel.fuelDepth / mtoft; // delta --> m
        FModel_ISU.fuelHeatContDead = FModel.fuelHeatContDead * BtutoJ * kgtolb; // h --> J/Kg

        //printf("ISU Fuel Model Input: %f, %f, %f, %f, %f, %f, %f, %f, %f\n", FModel_ISU.fuelSAV, FModel_ISU.fuelLoad, FModel_ISU.fuelMoistCont, FModel_ISU.mExt, FModel_ISU.fuelTotMinCont, FModel_ISU.fuelEfMinCont, FModel_ISU.fuelOvDensity, FModel_ISU.fuelDepth, FModel_ISU.fuelHeatContDead);
    
        // Calculate R0 in the two fuel models
        float R0 = ComputeROS0(FModel); // R0 in ft/min
        float R0_ISU = ComputeROS0_ISU(FModel_ISU); // R0 in m/s
        
        // Convert from Imperial Units to ISU
        float R0_new = R0 / mtoft / mintos;
        
        ComputeROS0_ISU(FModel);
        
        // Error
        float err = fabs(R0_new-R0_ISU);
        printf("Error: %f\n",err);
        
        return 0;
    } else {
        return 1;
    }
}






