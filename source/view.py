
from pylab import *
import os
import time

files = [f for f in os.listdir(".") if ".txt" in f]

ord = [f for f in files if "ord" in f]
exp = [f for f in files if "exp" in f]
red = [f for f in files if "red" in f]

# fig = plt.figure()

for o,e,r in zip(ord,exp,red):

    ofile = open(o,"r")
    efile = open(e,"r")
    rfile = open(r,"r")

    olines = ofile.read().split("\n")
    olines = [f.split() for f in olines]
    olines = olines[:-1]
    ox = [float(f[0]) for f in olines]
    oy = [float(f[1]) for f in olines]
    
    ox.append(ox[0])
    oy.append(oy[0])
    
    elines = efile.read().split("\n")
    elines = [f.split() for f in elines]
    elines = elines[:-1]
    ex = [float(f[0]) for f in elines]
    ey = [float(f[1]) for f in elines]
    ex.append(ex[0])
    ey.append(ey[0])


    rlines = rfile.read().split("\n")
    rlines = [f.split() for f in rlines]
    rlines = rlines[:-1]
    rx = [float(f[0]) for f in rlines]
    ry = [float(f[1]) for f in rlines]
    rx.append(rx[0])
    ry.append(ry[0])

    subplot(1,3,1)
    plot(ox,oy,'*-')
    title("ordenado" + o)
    
    subplot(1,3,2)
    plot(ex,ey,'*-')
    title("expandido" + e)

    subplot(1,3,3)
    plot(rx,ry,'*-')
    title("rediscretizado")
    
    show()



#    ax1 = fig.add_subplot(131)
#    ax1.plot(ox, oy, '-')

#    ax2 = fig.add_subplot(132)
#    ax2.plot(ox, oy, '-')

#    ax3 = fig.add_subplot(133)
#    ax3.plot(rx, ry, '-')


