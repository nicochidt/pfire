#include "perimeter.h"

// implementacion adaptada de https://gist.github.com/marodrig/10799703
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*! \brief Methods to define, expand and reorder the fire Perimeter
 *
 */
void rediscretizePerimeter(NODE *head, int MaxDist)
{   /*
     Scan the list and add nodes between points which distance is bigger than MaxDist.
     \param c1 Node pointer to the head of the list.
     \param c2 Integer with the maximum distance allowed between points.
     */
    NODE *next = head->next;
    NODE *prev = next;
    MaxDist *=MaxDist;
    
    float x1, x2, y1, y2;
    x1 = head->x;
    y1 = head->y;
    x2 = next->x;
    y2 = next->y;
    int size = 0;
    float distance;
    while(next != head)
    {
        distance = (x2-x1)*(x2-x1) + (y2 - y1)*(y2-y1);
        int i = 0;
        float xnew = x2;
        float ynew = y2;
        if (distance > MaxDist)
        {   i++;
            
            xnew = (xnew + x1) * 0.5;
            ynew = (ynew + y1) * 0.5;
            printf("agrego un punto en %f, %f\n",xnew, ynew);
            addNode(prev, xnew, ynew, &size);
            distance = (xnew-x1)*(xnew-x1) + (ynew - y1)*(ynew-y1);
        }
        prev = next;
        next = next->next;
        x1 = x2;
        y1 = y2;
        x2 = next->x;
        y2 = next->y;
    }
}

int getListSize(NODE * head)
{   /*
     Returns the size of a linked list. 
     \param c1 NODE pointer to the head node of the list. If more than one centinell node any node can be used.
     */
    if(!head) return 0;
    int size = 1;
    NODE *next = head ->next;
    while(next != head)
    {
        size++;
        next = next->next;
    }
    return size;
}

void listToVector(NODE *head, float *x, float *y)
{
    /* 
     Transform a linked list in two vectors x, y with the coordinates of the points. 
     \param c1 NODE pointer to the head node. (Input)
     Output Parameters:
     \param c2 Unallocated float pointer to save x coordinates. The functions allocates the needed space. 
     \param c3 Unallocated float pointer to save y coordinates. The functions allocates the needed space. 
     
     */
    int size = getListSize(head);
    if (size == 0) return;
    
    x = (float *)malloc(sizeof(float)*size);
    y = (float *)malloc(sizeof(float)*size);
    NODE *next = head;
    
    for (int i = 0; i < size; i ++)
    {
        x[i] = next->x;
        y[i] = next->y;
        next = next->next;
    }
}


NODE* createNode(float x, float y, NODE * next)
{   /*
     Method to create a new head node. Returns a NODE pointer.
     \param c1 x coordinate of the node.
     \param c2 y coordinate of the node.
     \param c3 (Optional) Pointer to the next link. If not specified next link is NULL.
     */
	NODE *head = (NODE *)malloc(sizeof(NODE));
	if (!head) return NULL;
	head-> x = x;
	head-> y = y;
	head-> next = next;
	return head;
}

void removeNode(NODE *prev, NODE *head, int *size)
{
    /*
     Remove the node to with the argument is pointing. In case the removed node is the head, a new head is defined, pointing to the next node in the list. 
     Does not return any value.
     \param c1 Pointer to the node previous to the one to be removed.
     \param c2 Pointer to the head node.
     \param c3 Pointer to an integer containing list size.
     */
	NODE * act  = prev->next;   // Node to be removed.
	NODE * next = act->next;    // Node to wich the actual node must point.
	if (act == head) {
		head = next;
	}
	free(act);
	prev->next = next;
	(*size)--;
	
}

NODE* addNode(NODE *prev, float x, float y, int *size)
{
    /* 
     Adds a node between the calling node and the next one. 
     Returns a NODE pointer.
     \param c1 Pointer to the node after wich a new node is going to be added. 
     \param c2 x coordinate of the new node. 
     \param c3 y coordinate of the new node. 
     \param c4 Pointer to an integer with the size of the list.
     */
	if (!prev) return NULL;
    NODE *new = createNode(x, y, prev->next);
	prev->next = new;   // Previous node points to the new node.
	(*size)++;
	return new;
}

bool isLast(NODE *node, NODE *head)
{   /* 
     Check if a node is the head node. (In a future version check if is any of the centinels nodes). 
     Returns a bool value. 
     \param c1 NODE pointer to the node to compare. 
     \param c2 NODE pointer to the head node. (In a future version will be a NODE **)
     */
	return node->next == head;
}

NODE *createPerimeter(float *x, float *y, int N)
{
    /* Creates a closed linked list from a pair of vectors representing a perimeter in x,y coordinates. The last node points to the head.
     Returns a NODE pointer to the head of the list. 
     \param c1 Float pointer with the x coordinates of the nodes.
     \param c2 Float pointer with the y coordinates of the nodes.
     \param c3 Integer with the size of the arrays to wich \param c1 and \param c2 points.
     */
	if (N < 2) return NULL;

	NODE * head = createNode(x[0],y[0],NULL);
	NODE * next;
	int size = 0;

	next = addNode(head, x[1], y[1], &size);	

	for (int i = 2; i < N; i++)
	{
		next = addNode(next, x[i], y[i], &size);
	}
	next->next = head;
	return head;
}


void printPerimeter(NODE *head)
{   /*
     Prints the perimeter represented by a linked list. For debbuging purposes.
     \param c1 NODE pointer to the perimeter's head node.
     */
	NODE *next = head->next;
	int i = 0;

	printf("Point: %d -- x: %f y: %f\n", 0, head->x, head->y);
	while (next != head)
	{	i++;
		printf("Point: %d -- x: %f y: %f\n", i, next->x, next->y);
		next = next->next;
	}
}


void visualizePerimeter(NODE *head, int radio)
{   /* 
     Prints a not-so fancy ASCII art representing the perimeter.
     \parameter c1 NODE pointer to the head node.
     
     */
	int size = 70;   // size of the matrix (adapted to the console line-size)
	char matrix[size][size];
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
			matrix[i][j] = ' ';
	}
	NODE *next = head->next;
	float delta = (2 * radio) / (float)size;
	int xcoord = (int)((head -> x )/ delta);
	int ycoord = (int)((head -> y )/ delta);

	matrix[xcoord + size / 2][ycoord + size / 2] = '0';
	char i='1';
	while (next != head)
	{   next = next->next;
		int xcoord = (int)((next -> x  ) / delta);
		int ycoord = (int)((next -> y  ) / delta);
	
		matrix[lround(xcoord+ size / 2.0)][lround(ycoord+ size / 2.0)] = i;
		i++;
		if (i > '9') i = '0';
		
	}
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
			printf(" %c", matrix[i][j]);
		printf("%d\n",i);
	}
}

void saveListToFile(NODE *head, char* filename)
{
    FILE *output;
    output = fopen(filename, "w");
    NODE *next = head->next;
    
    while (next != head)
    {
        fprintf(output,"%f %f\n",next->x, next->y);
        next = next->next;
    }
    fclose(output);
    
    
}

float normaVector(float x, float y)
{   /*
     Returns a floating point with the norm of the vector.
     \param c1 X coordinate of the vector.
     \param c2 Y coordinate of the vector.
     */
	return sqrt(x*x + y*y);
}

float findCosVectors(float vx0, float vy0, float vx1, float vy1){
	/*
     Finds the cosine of the angle between to vectors considering that: cos(\alpha) = (v1*v2)/(norm(v1)*norm(v2)).
     \param c1 X coordinate of vector 1
     \param c2 Y coordinate of vector 1
     \param c3 X coordinate of vector 2
     \param c4 Y coordinate of vector 2
     */
	return (vx0 * vx1 + vy0 * vy1)/(normaVector(vx0, vy0) * normaVector(vx1, vy1));
	
}

void swapNodes(NODE *p1, NODE *p2)
{   /*
     Swap the node P2 with P2->next. 
     \param c1 NODE pointer before the nodes to swap (needed to update the next value).
     \param c2 NODE pointer to swap with the node pointed by this.
     */
    
	NODE *p3 = p2 -> next;
	NODE *paux;
	p1 -> next = p3;
    paux = p3 -> next;
	p3 -> next = p2;
	p2 -> next = paux;

}

void reorderPerimeter(NODE *head)
{   /*
     Reorder the perimeter checking for crossed nodes. 
     \param c1 NODE pointer to the head of the perimeter. (In the furute is goint to be a NODE ** to all centinel nodes).
     */
	NODE * p1 = NULL, *p2 = NULL, *p3 = NULL;
	p2 = head ->next;
	p3 = p2 -> next;
	p1 = head;
   
	float nx, ny;
	float x0,y0,x1, x2, x3, y1, y2, y3;
	x0 = p2->x;
	y0 = p2->y;
	
	x1 = p1->x; x2=p2->x; x3=p3->x;
	y1 = p1->y; y2=p2->y; y3=p3->y;	
	
	double vx1, vx2, vy1, vy2;
	double threshold = cos(88.0 * M_PI / 180.0), alpha;  // threshold value to determine if two nodes are crossed

	char i = '0';  // for debbuging purposses
	int kk = 0;    // for debbuging purposses
    int kk2=0;
	while (p2 != head){
		vx1 = (x2 - x1); vx2 = (x3 - x2);
		vy1 = (y2 - y1); vy2 = (y3 - y2);
		alpha = findCosVectors(vx1, vy1, vx2, vy2);  // cos (v1, v2)
		if (alpha < - 0.70) {
			printf("Elimino alpha %f %c %d %f %f\n", alpha,i,kk, p1->x, p1->y);  // for debbuging purposses
			removeNode(p1, head, &kk2);
			//swapNodes(p1,p2);
			p2 = p1 -> next;
			p3 = p2 -> next;
		}
		else
		{
		
			x1 = x2;
			y1 = y2;
			x2 = x3;
			y2 = y3;
			p1 = p1 ->next;
			p2 = p2 ->next;
			p3 = p3 ->next;
			x3 = p3->x;
			y3 = p3->y;		
		}
		i++;            // for debbuging purposses
		if (i > '9'){   // for debbuging purposses
			i='0';
			kk++;
		}
	}	
}

void getNormalToSegment(float x, float y, float * out_x, float * out_y)
{
    /*
     Find a vector normal to the segment with direction vector (x,y). The resultant vector has unitary norm, points in the outside direction
     if the perimieter is arranged in clockwise form and is stored in the output parameters.
     Inputs:
     \param c1 X coordinate of the direction vector of the segment. 
     \param c2 Y coordinate of the direction vector of the segment. 
     Outputs:
     \param c3 X coordinate of the normal vector.
     \param c4 Y coordinate of the normal vector.
     */

	float nx, ny;
	if (y == 0){   // horizontal vector
		nx = 0.;
		ny = 1.;	
		
	} else if (x == 0) {   // vertical vector
		nx = 1.;
		ny = 0;
		
	} else {
		nx = 1.0;
		ny = -1.0 * x / y;	
	}
	
	if ((x * ny - y * nx) < 0) {   // this condition checks if the vector points out or into the perimeter.
		nx *= -1;
		ny *= -1;
	}
	
	float norma = normaVector(nx,ny);
	* out_x = nx / norma;
	* out_y = ny / norma;
	
}

void getNormalToPoint(float x1, float y1, float x2, float y2, float x3, float y3, float *out_x, float *out_y){
	/*
     Find a vector normal to the perimeter but related to a point instead of a segment. Necesary for the Huygens expansion method. 
     The way to calculate this is to find normal vector to the segments arriving to the point and calculating the vectorial sum. 
     Input parameters:
     \param c1 X coordinate of the first point (initial point of the first segment).
     \param c2 Y coordinate of the first point (initial point of the first segment).
     \param c3 X coordinate of the second point (end point of the first segment and initial point of the second segment).
     \param c4 Y coordinate of the second point (end point of the first segment and initial point of the second segment).     
     \param c5 X coordinate of the third point (end point of the second segment).
     \param c6 Y coordinate of the third point (end point of the second segment).
     Output parameters:
     \param c7 X coordinate of the normal vector to the point. 
     \param c8 Y coordinate of the normal vector to the point.
     */
	float vx1, vy1, vx2, vy2;
	vx1 = (x2 - x1);
	vy1 = (y2 - y1);
	vx2 = (x3 - x2);
	vy2 = (y3 - y2);
			
	float n1x, n1y, n2x, n2y;

	getNormalToSegment(vx1, vy1, &n1x, &n1y);
	
	getNormalToSegment(vx2, vy2, &n2x, &n2y);
		
	float nx, ny, norma; 
	nx = n1x + n2x;
	ny = n1y + n2y;
	norma = normaVector(nx,ny);
	*out_x = nx / norma;
	*out_y = ny / norma;	
	
	
}

void expandPerimeter (NODE *head){
	/*
     Expand each point of the perimeter represented by a closed linked list. 
     This version uses a expansion in the normal direction to each point and the length of the vector is given by a random number
     (in the future this number will be replaced for the rate of spread). 
     \param c1 NODE pointer to the head node of the perimeter.
     */
    /* TODO: replace the random number for the Rothermel's R */

	NODE * p1 = NULL, *p2 = NULL, *p3 = NULL;
	p2 = head ->next;
	p3 = p2 -> next;
	p1 = head;
	
    
	float nx, ny;
	float x0,y0,x1, x2, x3, y1, y2, y3;
	x0 = p2->x;
	y0 = p2->y;
	
	x1 = p1->x; x2=p2->x; x3=p3->x;
	y1 = p1->y; y2=p2->y; y3=p3->y;	
	
	double R;
	
	while (p2 != head){
		
		getNormalToPoint(x1, y1, x2, y2, x3, y3, &nx, &ny);

		R = 1.50 * rand() / (double)RAND_MAX;  // rate of spread
		p2 -> x = p2 -> x + R * nx;
		p2 -> y = p2 -> y + R * ny;
		
		x1 = x2;
		y1 = y2;
		x2 = x3;
		y2 = y3;
		p1 = p1 ->next;
		p2 = p2 ->next;
		p3 = p3 ->next;
		x3 = p3->x;
		y3 = p3->y;	
	}	
	// expando el head
		getNormalToPoint(x1, y1, x2, y2, x0, y0, &nx, &ny);

	R = .5 * rand() / (double)RAND_MAX;  // rate of spread
	head -> x = head -> x + R * nx;
	head -> y = head -> y + R * ny;

}

int main(int argc, char const *argv[])
{   
	int points = 40;
	float radio =12;
	float N = 2.0 * M_PI / (float)points;
	float x[points], y[points];
	
	for (int i = 0; i < points; i++)
	{
		x[i] = 0 + radio * cos(-1.0 * N * i);
		y[i] = 0 + radio * sin(-1.0 * N * i);		

	}
	
srand(time(NULL));
	//getNormalToSegment(vx, vy, &nx, &ny);
	//printf("vx: %f, vy: %f, nx: %f, ny: %f\n", vx, vy, nx, ny);
	
	
	
	float x2[4], y2[4];
	x2[0] = -10;
	x2[1] =10;
	x2[2] =10;
	x2[3] =-10;
	y2[0] = 10;
	y2[1] =10;
	y2[2] =-10;
	y2[3] =-10;
	
		NODE * head = createPerimeter(x, y, points);
//	NODE * head = createPerimeter(x2, y2, 4);
//	printPerimeter(head);
    char name[256];
    
	for (int i = 1; i < 7; i++){
//		visualizePerimeter(head, radio + 10);
		expandPerimeter(head);
        
        sprintf(name, "exp%d.txt",i);
        saveListToFile(head, name);
        
//		visualizePerimeter(head, radio + 10);
		reorderPerimeter(head);
        sprintf(name, "ord%d.txt",i);
        saveListToFile(head, name);
        
        rediscretizePerimeter(head, 2);
        sprintf(name, "red%d.txt",i);
        saveListToFile(head, name);
//		visualizePerimeter(head, radio + 10);
		printf("\n\n");
	
        //	printPerimeter(head);
		
		//printf("(x,y): %f, %f\n", head->x, head->y);

	}
	
	
	
	
	return 0;
}






