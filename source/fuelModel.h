#pragma once
#include <stdlib.h>


typedef struct FuelModel {
	int modelNumber;                        // 0-255?
	// Variables que pide el WRF-SFIRE
	float wnAdFctr;                         // a: wind adjustment factor
	float fuelWnHgt;                        // z_f: fuel wind height
	float fuelRgHgt;                        // z_0: fuel roughness height
	float fuelweight;                       // \omega: fuel weight (s) --> Fuel burn time ~ (\omega / 0.8514)
	float fuelLoad;                         // \omega_l: total fuel load (kg/m^2)
	float fuelDepth;                        // \delta: fuel depth (m)
	float mExt;                             // M_x: moisture contect of extinction (1)
	float fuelSAV;                          // \sigma: fuel particle surface to area volume ratio (1/m)
	float fuelOvDensity;                    // \rho_P: fuel ovendry particle density (kg/m^3)
	float fuelTotMinCont;                   // S_T: fuel particle total mineral content (1)
	float fuelEfMinCont;                    // S_e: fuel particle efective mineral content (1)
	float fuelHeatContDead;                 // h: fuel heat contents of dry fuel (J/Kg)
	float fuelMoistCont;                    // M_f: fuel particule moisture content (1)
	
	// Variables que pide el FARSITE (sin repetir las que pide el WRF)
	float fuelM1H, fuelM10H, fuelM100H;     // humedad a 1, 10 y 100 horas (tons/ha)??
	float fuelLiveH;                        // tons/ha
	float fuelLiveW;                        // tons/ha
	float fuelHSAV;                         // (1/m)
	float fuelWSAV;                         // (1/m)
	float fuelHeatContLive;                 // h fuel heat contents of dry fuel (J/Kg)

} FUELMODEL;



