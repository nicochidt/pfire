/*==============================================================================================================
    PFire Simulator - BaseMap.c
=================================================================================================================
    PFire it's a forest fire simulator developed to be executed in multi many core environments.
    It's a project that born at (ACSO/CAOS) department in the Unversity Autonomous of Barcelona (UAB).
=================================================================================================================
    Version: V0 (in progress...) Betta Version in Serial execution
=================================================================================================================
    Authors:
        Àngel Farguell
        Nicolas Chiaraviglio
        Lucas Pelegrin
===============================================================================================================*/
#include "BaseMap.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
/*=================================================================================================================
    Function:   BaseMap_Initialization

    Description:
    Sets the model data( Elevation, Slope, Aspect )from the input files.
=================================================================================================================*/
void BaseMap_Initialization(struct BASEMAP *BaseMap ,char *Elevation_file){

    BaseMap_ElevationMapInitialization( BaseMap , Elevation_file);
    printf("BaseMap: Elevation Map Initialization[OK]\n");
    BaseMap_SlopeMapInitialization( BaseMap );
    printf("BaseMap: Slope Map Initialization[OK]\n");
    BaseMap_AspectMapInitialization( BaseMap );
    printf("BaseMap: Aspect Map Initialization[OK]\n");
    BaseMap_FuelMapInitialization( BaseMap );
    printf("BaseMap: Fuel Map Initialization[OK]\n");


}
/*=================================================================================================================
    Function:   BaseMap_ElevationMapInitialization

    Description:
    Sets the elvation data model from the input file.
=================================================================================================================*/
void BaseMap_ElevationMapInitialization(struct BASEMAP *BaseMap ,char *Elevation_file){
    int x;
    int y;
    int ValueReaded;
    int NoDataValue;
    int Error = 0;
    long int count = 0;
    long int MapPoints;
    FILE *File = fopen( Elevation_file, "r");
    int Buffersize = sizeof(char) * MAXBUFFERSIZE ;
    char *Buffer_parameter;
    char *Buffer_data;
    Buffer_parameter = (char *)malloc(Buffersize);
    Buffer_data      = (char *)malloc(Buffersize);
    if ( File == NULL ) {
        printf("ERROR ( BaseMap - ElevationMapInitialization ) : File == NULL\n");
        exit(-1);
    }
    //HEADER SECTION======
    //Reading the values
    while(!feof(File) && count < MAXHEADERLINESELEVATIONFILE  && !Error ){
        Error = 1;
        count = count + 1 ;
        fscanf(File,"%s",Buffer_parameter);
        fscanf(File,"%s",Buffer_data);
        if ( !strcmp(Buffer_parameter,"ncols") ){
            Error = 0;
            sscanf(Buffer_data,"%d", &BaseMap->X_Cells);
        }
        if ( !strcmp(Buffer_parameter,"nrows") ){
            Error = 0;
            sscanf(Buffer_data,"%d", &BaseMap->Y_Cells);
        }
        if ( !strcmp(Buffer_parameter,"xllcorner") ){
            Error = 0;
            sscanf(Buffer_data,"%lf", &BaseMap->X_Position);
        }
        if ( !strcmp(Buffer_parameter,"yllcorner") ){
            Error = 0;
            sscanf(Buffer_data,"%lf", &BaseMap->Y_Position);
        }
        if ( !strcmp(Buffer_parameter,"cellsize") ){
            Error = 0;
            sscanf(Buffer_data,"%f", &BaseMap->CellSize);
        }
        if ( !strcmp(Buffer_parameter,"NODATA_value") ){
            Error = 0;
            sscanf(Buffer_data,"%d", &BaseMap->NoDataValue);
            sscanf(Buffer_data,"%d", &NoDataValue);
        }
        if ( Error ){
            printf("ERROR ( BaseMap - ElevationMapInitialization ): header error\n");
            exit(-1);
        }

    }

    //DATA SECTION======
    MapPoints= BaseMap->X_Cells * BaseMap->Y_Cells;
            //Alocating Memory for Maps
    BaseMap->ElevationMap   = (int *)   malloc(sizeof(int )   * MapPoints);
    BaseMap->SlopeMap       = (float *) malloc(sizeof(float ) * MapPoints);
    BaseMap->AspectMap      = (float *) malloc(sizeof(float ) * MapPoints);
    BaseMap->FuelMap        = (int *)   malloc(sizeof(int )   * MapPoints);

            //Initialization of Elevation map & Reading values from File
    for (y = 0; y < BaseMap->Y_Cells; y ++){
        for (x = 0; x < BaseMap->X_Cells; x ++){
            if (feof(File)){
                printf("ERROR ( BaseMap - ElevationMapInitialization ) : Values expected: %li, Values counted: %li \n",MapPoints, count+1 );
                exit(-1);
            }
            fscanf(File,"%s",Buffer_data);
            sscanf(Buffer_data,"%d", &ValueReaded);
            if ( ValueReaded == NoDataValue){
                printf("ERROR ( BaseMap - ElevationMapInitialization ) : NODATA_value found inside the map. It must not containg null values\n");
                exit(-1);
            }
            BaseMap->ElevationMap[y * BaseMap->Y_Cells + x]=ValueReaded;
        }
    }
}
/*=================================================================================================================
    Function:   BaseMap_SlopeMapInitialization

    Description:
        Create slope map without doing the bounds.

    Source: http://webhelp.esri.com/arcgisdesktop/9.2/index.cfm?TopicName=How%20Slope%20works

    The slope in degrees is:
        ATAN (rise_run) * (180 / pi )
    Where rise_run is:
        rise_run = √ ( [dz/dx]2 + [dz/dy]2 ]
    The rate of change in the x direction:
        [dz/dx] = ((NE + 2*E + SE) - (NW + 2*W + SW) / (8 * x_cell_size)
    The rate of change in the y direction:
        [dz/dy] = ((SW + 2*S + E) - (NW + 2*N + NE)) / (8 * y_cell_size)
    Cell:
     |NW|N|NE|
     |W |C| E|
     |SW|S|SE|

    Reference:
    - Burrough, P. A. and McDonell, R.A., 1998. Principles of Geographical Information Systems
    (Oxford University Press, New York), p. 190.
=================================================================================================================*/
void BaseMap_SlopeMapInitialization(struct BASEMAP *BaseMap){
    int x;
    int y;
    int ymax = BaseMap->Y_Cells;
    int xmax = BaseMap->X_Cells;
    float RadToDegree = 180 / M_PI;
    float N,NE,E,SE,S,SW,W,NW;
    float riserun;
    float x_change,y_change;

    //Calculating bounds Map - Vertical=============
    /*y = BaseMap->Y_Cells-1;
    for( x = 1; x < xmax-1; x++){ // Y-Bound

    }
    //Calculating bounds Map - Horizontal=============
    x = BaseMap->X_Cells-1;
    for( y = 1; y < ymax-1; y++){ // X-Bound

    }*/

    //Calculating inner Map==========================
    for (y = 1; y < ymax - 1 ; y ++){
        for (x = 1; x < xmax - 1 ; x ++) {
            N  = BaseMap->ElevationMap[(y-1) * ymax + x    ];
            NE = BaseMap->ElevationMap[(y-1) * ymax + (x+1)];
            E  = BaseMap->ElevationMap[y     * ymax + (x+1)];
            SE = BaseMap->ElevationMap[(y+1) * ymax + (x+1)];
            S  = BaseMap->ElevationMap[(y+1) * ymax + x    ];
            SW = BaseMap->ElevationMap[(y+1) * ymax + (x-1)];
            W  = BaseMap->ElevationMap[y     * ymax + (x-1)];
            NW = BaseMap->ElevationMap[(y-1) * ymax + (x-1)];
            x_change = ((NE + 2*E + SE) - (NW + 2*W + SW)) / (8 * BaseMap->CellSize);
            y_change = ((SW + 2*S + E) - (NW + 2*N + NE)) / (8 * BaseMap->CellSize);
            riserun  = sqrtf(powf(x_change,2)+powf(y_change,2));
            BaseMap->SlopeMap[y * ymax + x] = atanf(riserun) * RadToDegree;
        }
    }
}
/*=================================================================================================================
    INCOMPLETED

    Function:   BaseMap_AspectMapInitialization

    Description:
        Create aspect map without doing the bounds.

    Source: http://webhelp.esri.com/arcgisdesktop/9.2/index.cfm?TopicName=How%20Aspect%20works

    The aspect in degrees is:
        aspect = (180 / pi) * atan2 ([dz/dy], -[dz/dx])
    The rate of change in the x direction:
        [dz/dx] = ((NE + 2*E + SE) - (NW + 2*W + SW) / (8 * x_cell_size)
    The rate of change in the y direction:
        [dz/dy] = ((SW + 2*S + E) - (NW + 2*N + NE)) / (8 * y_cell_size)
    Cell:
     |NW|N|NE|
     |W |C| E|
     |SW|S|SE|

    The aspect value is then converted to compass direction values (0–360 degrees)

    Reference:
    - Burrough, P. A. and McDonell, R.A., 1998. Principles of Geographical Information Systems
    (Oxford University Press, New York), p. 190.

=================================================================================================================*/
void BaseMap_AspectMapInitialization(struct BASEMAP *BaseMap){
    int x;
    int y;
    int ymax = BaseMap->Y_Cells;
    int xmax = BaseMap->X_Cells;
    float RadToDegree = 180 / M_PI;
    float N,NE,E,SE,S,SW,W,NW,aspect;
    float x_change,y_change;


    //Calculating bounds Map - Vertical=============
    /*y = BaseMap->Y_Cells-1;
    for( x = 1; x < xmax-1; x++){ // Y-Bound

    }
    //Calculating bounds Map - Horizontal=============
    x = BaseMap->X_Cells-1;
    for( y = 1; y < ymax-1; y++){ // X-Bound

    }*/

    //Calculating inner Map==========================
    for (y = 1; y < BaseMap->Y_Cells - 1; y ++){
        for (x = 1; x < BaseMap->X_Cells - 1; x ++) {
            N  = BaseMap->ElevationMap[(y-1) * ymax + x    ];
            NE = BaseMap->ElevationMap[(y-1) * ymax + (x+1)];
            E  = BaseMap->ElevationMap[y     * ymax + (x+1)];
            SE = BaseMap->ElevationMap[(y+1) * ymax + (x+1)];
            S  = BaseMap->ElevationMap[(y+1) * ymax + x    ];
            SW = BaseMap->ElevationMap[(y+1) * ymax + (x-1)];
            W  = BaseMap->ElevationMap[y     * ymax + (x-1)];
            NW = BaseMap->ElevationMap[(y-1) * ymax + (x-1)];
            x_change = ((NE + 2*E + SE) - (NW + 2*W + SW)) / 8;
            y_change = ((SW + 2*S + E) - (NW + 2*N + NE))  / 8;
            aspect = atan2f(y_change,-x_change) * RadToDegree;
            if ( aspect < 0) {
                aspect = 90.0 - aspect;
            }else{
                if ( aspect > 90.0) {
                    aspect = 360.0 - aspect + 90;
                }else{
                    aspect = 90.0 - aspect;
                }
            }
            BaseMap->AspectMap[y * ymax + x] = aspect;
        }
    }
}
/*=================================================================================================================
    Function:   BaseMap_FuelMapInitialization

    INCOMPLETED, temporal function that sets fuel map at 1 model.

    Description:
=================================================================================================================*/
void BaseMap_FuelMapInitialization(struct BASEMAP *BaseMap){
    int x;
    int y;
    for ( y = 0 ; y < BaseMap->Y_Cells ; y++ ){
        for ( x = 0 ; x < BaseMap->X_Cells ; x++){
            BaseMap->FuelMap[y * BaseMap->Y_Cells + x] = 1 ;
        }

    }
}
/*=================================================================================================================
    Function:   BaseMap_ElevationMapInitialization

    Description:
    Set free the memory allocated in the Initialization part.
=================================================================================================================*/
void BaseMap_BaseMapFreeMemory(struct BASEMAP *BaseMap){

    free(BaseMap->ElevationMap  );
    free(BaseMap->SlopeMap      );
    free(BaseMap->AspectMap     );
    free(BaseMap->FuelMap       );

}


